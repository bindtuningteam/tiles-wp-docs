1. Open the page where you've added the web part; 
2. Click on **Bindtuning**, and then **Edit Web parts** - the edit panel will appear from the left side of the web part;
3. Click on the ➕ (plus) icon to add a new tile;
4. On the **Tile Type**, choose **Video**;

<p class="alert alert-success">The video tile supports YouTube videos, Vimeo videos and Portal videos.</p>

![tilessettingsglossary_7.png](../images/classic/22.connect_video.png)


### Connect to a video

- **URL:** Type here the URL of the video that you want to add.
- **Title:** The title of the video. If no tile is provided, it will be the same as the URL;



To enrich more the Tile component, you need to configure the other sections that we explain below: 

- [Front Tiles and Back Tiles settings](./frontback)
- [Animations Settings](./animation)

After setting everything up, click on **Publish**. You can also preview the tile on the page before saving it, by clicking on the **Preview** button.