1. Open the page where you've added the web part; 
2. Click on **Bindtuning**, and then **Edit Web parts** - the edit panel will appear from the left side of the web part;
3. Click on the ➕ (plus) icon to add a new tile;
4. On the **Tile Type**, choose **Document Library**;

![document_library](../images/classic/30.document_library.png)

### Connect to your Document Library

Here you can define the document library that will be displayed inside a Modal window.

- **URL:** Type here the URL of the document library that you want to add.
- **Title:** Type here the title for the Modal Window. If you don't enter a title, the URL will be defined as title.

To enrich more the Tile component, you need to configure the other sections that we explain below: 

- [Front Tiles and Back Tiles settings](./frontback)
- [Animations Settings](./animation)

After setting everything up, click on **Publish**. You can also preview the tile on the page before saving it, by clicking on the **Preview** button.