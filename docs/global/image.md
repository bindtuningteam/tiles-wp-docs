When you select the option to be an image it will allow you to:

- **Insert an URL** of the image location directly to the text area;
- **Browse on your SharePoint site** and pick a picture.

![browse_image](../images/classic/26.browse_image.png)

There you will be presented with a panel where you can choose from your SharePoint site collection an image to insert on the Tile area. This will display all your Libraries but only the images will be displayed.

![image_picker](../images/classic/27.image_picker.png)


### Upload image

You can also upload your image for the select library by selecting the icon at the top to **Upload File** or by dragging the image for the area. Note this will upload the image directly to Library that you've selected. 


![addpicture](../images/classic/06.addpicture.gif)
