### Flip Animation

![animationsettings](../images/classic/25.animationsettings.png)

When the tile supports a front and back tile, you can set the animation you want your tile to show when switching between the front and back tile.

<p class="alert alert-success">When <strong>none</strong> is selected, the back tile will never appear.</p>

___
### Animation Interval 

Here you can define the seconds of the interval between the flip animations.

If the interval is **0**, the tile will only flip every time you place the cursor on the tile. If you set any other value, the tile will animate, revealing the back side or the next slide, every couple of seconds. 

<p class="alert alert-success">If you don't enter any value, the tile will animate every 5 seconds.</p>