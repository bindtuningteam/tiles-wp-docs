1. Open the page where you've added the web part; 
2. Click on **Bindtuning**, and then **Edit Web parts** - the edit panel will appear from the left side of the web part;
3. Click on the ➕ (plus) icon to add a new tile;
4. On the **Tile Type**, choose **Weather**;

### Default location

With Default Location, the tile will read the forecast depending on the city added.You can check the name of the city which you intend to show on the Web Part on the <a href="https://openweathermap.org/find" target="_blank">next link</a>.

![weather](../images/classic/30.weather.png)

___
### Dynamic Location

If you intend to have the Weather of your current location, you need to enable that option on the Web Part. 

<p class="alert alert-info"><b>Note</b>: You will be requested from the Browser to allow to detect your location.</p>

___
### Measurements

Choose if you want the tile to display temperature in **Celsius** or **Fahrenheit** units.

___
### Forecast

You can choose to have forecasts displayed per day or weekly.

___
### Tile Background

Choose a **Color** or **Image** for the tile background. For more information on how to use:
    
- The color picker, refer to <a href="../format/#color" target="_blank">this section</a>.
- The image picker, refer to <a href="../image" target="_blank">this section</a>.

___
### Text Color

You can insert the Hex code of the color you intend to use. If you want to pick a custom color, you can open the color picker by pressing the <i class="fa fa-crosshairs"></i> icon. Colors selected through here are static and will always show the same regardless of the page.