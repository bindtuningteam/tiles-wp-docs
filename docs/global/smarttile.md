Let’s say we want to map the BindTuning Tiles with a custom SharePoint List, with the Smart Tile will have this flexibility. The created Tile will be used to create a map of the columns content that will be shown on the page based on the Custom List Items.


1. Open the page where you've added the web part; 
2. Click on **Bindtuning**, and then **Edit Web parts** - the edit panel will appear from the left side of the web part;
3. Click on the ➕ (plus) icon to add a new tile;
4. On the **Tile Type**, choose **Smart Tile**;
	
![smarttile_addlist.gif](../images/classic/07.smart_tile.gif)
___
### Connect to a Data Source

The **Source list** is where your information will be pulled from. On the **Front Tiles Options**, we'll create the mapping used to present the information.

1. Click on **+ Add List** first;
2. On the first text box insert one of the two:

	- **The URL of the list you created** 
	- **A URL of a site collection**	

3. Choose your **Filtering Options** to filter your list elements. You can choose to have:

    - **No Filters** - You show all the items on the list.
    - **CAML Query** - You can query the list to just show items based on the query built. 
    - **Select a View of the list** - You can pick the List View created and just show those elements on the list.


![smarttile_addlist.gif](../images/classic/31.datasource.png)

<p class="alert alert-success">Need to add more lists? You can do it by clicking on <strong>Add List</strong>.</p>

___
### Item Sorting Options

![webpartproperties_8.png](../images/classic/07.sortingitems.png)

<p class="alert alert-info">By default, the Web Part will sort the items by ID</a>.</p>

You will need to type in the internal name of the list column on the **Order by** text box - we will sort the tiles according to the value of the cell name you enter.

Here is what you need to do: 

1. Access **Site contents** and open your list;
2. On the top menu, click on **List** and then **List Settings** (or **Settings** > **List Settings**);
3. On the **Columns** section, click to open the colum name you want to use;
4. Inside, on the URL look for **"...Field=..."**.  
5. Copy the internal name;

    ![webpartproperties_18.png](../images/classic/17.internalcolumnname.png) 
	
6. Now paste the name on the text box;
7. Choose the order, **Ascending** or **Descending**. 

With **Add Order By**, you can add more sorting options. 
___
### Setup a Link

![browse_image](../images/classic/28.link.png)

You can set the link for your Smart Tile. It will open every time an user clicks on the tile. If you don't need a link on the Smart Tile, you can leave this section empty. 

You can also decide whether a new window should be opened or not by enabling the option **Open link in new window**. 

If you intend you can map a column from the list you've connected on **Source list**. To use that column as **link**, you need to place the column's internal name in brackets: <b>{{FIELD_NAME}}</b>.

Here is what you need to do to check your column name: 

1. Access **Site contents** and open your list;
2. On the top menu, click on **List** and then **List Settings** (or **Settings** > **List Settings**);
3. On the **Columns** section, click to open the colum name you want to use;
4. Inside, on the URL look for **"...Field=..."**.  
5. Copy the internal name;
6. Now paste the name on the text box;

![webpartproperties_18.png](../images/classic/17.internalcolumnname.png) 

___
### Front Tile Settings

<strong>For the background</strong>, you can use a field from the source list to add an image or color. Simply choose the image option in the background field and place the column's internal name in brackets: <strong>{{FIELD_NAME}}</strong>, as shown in the image below.

![smarttilebackground](../images/classic/32.smarttilebackground.png)

<strong>For the content</strong>, you can use a field from the queried list to add text. Simply type the column's internal name in brackets: <b>{{FIELD_NAME}}</b>, as shown in the image below.

![smarttilecontent](../images/classic/33.smarttilecontent.png)

Here is what you need to do to check your column names: 

1. Access **Site contents** and open your list;
2. On the top menu, click on **List** and then **List Settings** (or **Settings** > **List Settings**);
3. On the **Columns** section, click to open the colum name you want to use;
4. Inside, on the URL look for **"...Field=..."**.  
5. Copy the internal name;
6. Now paste the name on the text box;

<p class="alert alert-info">You can still use the <a href="./format" target="_blank">MarkDown</a> syntax as normal.</p>

![webpartproperties_18.png](../images/classic/17.internalcolumnname.png) 

The rest is up to you - fill out the remaining options as you want. You can check out what you can do in each setting on the [Tiles Setting](./tilessettings) and [Animations Settings](./animation) sections of this User Guide;

After setting everything up, click on **Publish**. You can also preview the tile on the page before saving it, by clicking on the **Preview** button.

___ 
### How to use CAML Query

You can use a free 3rd party software for creating the query, like this one👉<a href="http://www.u2u.be/software/" target="_blank">http://www.u2u.be/software/</a>. Then you can copy the query and use it as a filter. Below is an example with multiple filters allowed of a query you can have.

   <p class="alert alert-success"> If you’re using SharePoint 2013, install the U2U CAML Query Builder for SharePoint 2013 (Standalone Edition).</p>

    <View>
    <Query>
        <Where>
            <And>// have to meet this 2 conditions
                <Eq> //Equal
                    <FieldRef Name='PromotedState' />
                    <Value Type='Number'>2</Value>
                </Eq>
                <Leq> //Lower or Equal
                    <FieldRef Name="Created" />
                    <Value Type="DateTime" Offset="-90"> // -90 days from today
                        <Today />
                    </Value>
                </Leq>
            </And>
        </Where>
        <OrderBy>
            <FieldRef Name='Created' Ascending='False'></FieldRef> // Most Recent to older created element
        </OrderBy>
    </Query>
    <RowLimit>3</RowLimit> // Number of items that you want to show
    </View>