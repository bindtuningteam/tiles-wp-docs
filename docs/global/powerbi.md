1. Open the page where you've added the web part; 
2. Click on **Bindtuning**, and then **Edit Web parts** - the edit panel will appear from the left side of the web part;
3. Click on the ➕ (plus) icon to add a new tile;
4. On the **Tile Type**, choose **Power BI**;

![save_buttons.png](../images/classic/21.power_bi.png)

### Connect to a Power BI dashboard

- **URL:** This is where you paste the URL of a public dashboard to be shown on this tile.

After setting everything up, click on **Publish**. You can also preview the tile on the page before saving it, by clicking on the **Preview** button.