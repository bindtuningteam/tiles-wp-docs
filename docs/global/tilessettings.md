![tilessettingsglossary](../images/classic/23.tilessettingsglossary.png)


### Tile Shape

First up is selecting your new Tile shape. Choose from small square, large square, horizontal rectangle or vertical. **The default tile shape is the small square.**


### Tile Order

You can easily choose the order for each Tile. This order is based on a numerical order.  

	- 0 - this tile will be the first one to appear
	- 10 - this tile will be the second one to appear
	- 20 - this tile will be the third one to appear
	....

<p class="alert alert-success">Tiles with the same value are ordered based on date of creation (older tiles first). We recommend to use increments of 10 so then you can add Tiles on middle easily.</p>


### Tile Visible

If you want the tile to be visible on your site choose **Active**. If you don't want it visible choose the **Inactive** option.

### Tile Type

This is a very important field. Here is where you get to define the type of your new Tile. You can choose from:

- [Plain](./plain)
- [Link](./link)
- [Document](./document)
- [Document Library](./documentlibrary)
- [Video](./video)
- [Smart Tile](./smarttile)
- [Weather Tile](./weather)
- [Power BI](./powerbi)

### Target Users

Here you can choose who will have permissions to see the tile you are adding.

<p class="alert alert-success">If you plan to use Active Directory groups to target your content, please check <a href="https://support.bindtuning.com/hc/en-us/articles/360015525971-AD-support-with-BindTuning-web-parts-" target="_blank">this article</a>.</p>
