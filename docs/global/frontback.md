![frontandback](../images/classic/24.frontandback.png)

Depending on the type of the tile you choose, you can have a front tile and a back tile. 

<p class=" alert alert-success">The front tile is displayed by default and the back tile is displayed when the cursor hovers the tile or every few seconds.</p>

### Tile Background

Choose a **Color** or **Image** for the tile background.

For more information on how to use:
    
- The color picker, refer to <a href="../format/#color" target="_blank">this section</a>.
- The image picker, refer to <a href="../image" target="_blank">this section</a>.

___
### Caption Positions

Switch between the **TOP**, **CENTER** and **BOTTOM** tab to add the content to the top of tile, the center or the bottom of the tile. 

___
### Content

Type the text you want for your tile. You can use the format buttons to define headings, alignment, weight, colors and add icons to the text. or more information about the controls, check the [next link](./format.md)

You will need to use Markdown to add text to your tile. If you don't know much about Markdown read more about it on our <a href="https://support.bindtuning.com/hc/en-us/articles/213784643" target="_blank">Markdown syntax in BindTuning</a> article.

___
### Caption Background

Defines a background for your caption. Tiles can have 3 captions, one at the **TOP**, one at **CENTER** and another at the **BOTTOM** of the tile - depending on where you add your content.