![webpartproperties_8.png](../images/classic/07.sortingitems.png)

<p class="alert alert-info">By default, this option contains the <b>Order By</b> on the column Sequence of the Tiles list, which represent an option to insert a tiles on a page with an certain order. For more information about this option check the <a href="../../global/tilessettings">Tiles Settings</a>.</p>

Here you can define what will be the order of your tiles using the internal name of one of your list colunms name. 

You will need to type in the internal name of the list column on the **Order by** text box - we will sort the tiles according to the value of the cell name you enter.

Here is what you need to do: 

1. Access **Site contents** and open your list;

2. On the top menu, click on **List** and then **List Settings** (or **Settings** > **List Settings**);

3. On the **Columns** section, click to open the colum name you want to use;

4. Inside, on the URL look for **"...Field=..."**.  
5. Copy the internal name;

	![webpartproperties_18.png](../images/classic/17.internalcolumnname.png) 
	
6. Now paste the name on the text box;

7. Choose the order, **Ascending** or **Descending**. 

With **Add Order By**, you can add more sorting options. 