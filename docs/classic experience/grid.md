### Max Tiles Per Row

Since BindTuning tiles can be displayed in a grid view, you can choose how many number of tiles you want to see in each row. You can choose between **Flexible**, which will make your tiles adapt to the space they have on the web part container, or a number limit, from **01 to 10 tiles** per row.

<p class="alert alert-success">Higher numbers  will result in bigger tiles where as smaller numbers will show smaller tiles.</p>

___
### Tiles Spacing

You can also set the spacing between the tiles. You can choose from:

- Zero 
- Five
- Ten 
- Twenty 

___
### Tiles Alignment

Choose between:

- **Grid alignment**, if you want the tiles to be displayed in a perfect alignment (Windows style).
- **Wall alignment**, if you want the tiles to adapt to the space they have in the container, similar to a Pinterest wall.

___
### Grid Size

This option is only available when you've the **Flexible** option active on **Max Tiles Per Row**. Activating the **Locked** option prevents the tiles from making small adjustments to their size in order to fill all horizontal space available. By default, tiles will always attempt to fill as much horizontal space as possible inside the web part container by adjusting their position and size.

___
### Mininum Tile Size

Here you can set the minimum tile size you want your tiles to have. You can choose between:

- **Extra Tiny** (100px)
- **Tiny** (150px)
- **Small** (175px)
- **Medium** (200px)
- **Large** (250px)

![Messages](../images/classic/12.grid.png)