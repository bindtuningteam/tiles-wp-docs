On the Web Part Properties panel, you've multiple options which you can edit for different configuration of the Web Part.
 
- [Tiles List](./tiles.md)
- [Item Sorting Options](./sorting.md)
- [Grid Settings](./grid.md)
- [Advanced Options](./advanced.md)
- [Performance](./performance.md)
- [Web Part Appearance](./appearance.md)
- [Publishing Workflow](./workflow.md)
- [Weather Settings](./weather.md)
- [Web Part Messages](./message.md)

![Settings](../images/classic/01.generaloptions.gif)

The global settings form let you apply options to **all the web parts** on the page at once. To use the form, follow the steps:

- [Configure Global Settings](./globalsettings.md)

![global_01_tab.PNG](../images/classic/05.globalsettings.png)