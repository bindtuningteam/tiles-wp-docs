### Enable Publishing Workflow

The Publishing Workflow let's you save the web part on draft, submit it for review and publish it. With this option enabled you **Save as Draft**, **Send for Review** or **Publish**. 


### Content Approvers

Insert the emails of the users you want to give permissions to publish content that's in draft or was submitted to review.

<p class="alert alert-info">If a web part content is save as draft or submitted to review it will only appear in edit mode.</p>

<p class="alert alert-info">Web Part content in draft will only appear to the person who created the content.</p>

![Messages](../images/classic/14.publishingworkflow.png)