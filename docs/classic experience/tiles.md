### Create a list 

This list is what you will use to store your Tiles.

Let's create your list:

1. Click on **Settings** ⚙️ and then on **Add an app**;

	![configuration_3.png](../images/classic/18.addalist.png)

2. On the search box type **BT**, look for the Tiles BT and click;	
4. Name the list and click **Create**. List created;

	![addalist.png](../images/classic/02.addalist.gif)

___
### Tiles List

Here you will need to connect your web part with a SharePoint list. Without it, the web part will not function and an notification message will appear. Let's get to it:

1. On **Tiles List**, click **Add List**

2. On the first text box insert one of the two:

	- **The URL of the list you created** 
	- **A URL of a site collection**	 

	<p class="alert alert-warning">Use relative paths for this field. So instead of using an URL like <strong>https://company.sharepoint.com/sites/Home</strong>, you should use something like <strong>/sites/Home/</strong>.</p>

3. Second box, select the list you have created to be used if not already selected;

4. Choose your **Filtering Options** to filter your list elements. You can choose to have:

    - **No Filters** - You show all the items on the list.
    - **CAML Query** - You can query the list to just show items based on the query built.
    - **Select a View of the list** - You can pick the List View created and just show those elements on the list.


5. When you're done, click on the 💾(save) icon to save.

__
### How to use CAML Query

You can use a free 3rd party software for creating the query, like this one👉<a href="http://www.u2u.be/software/" target="_blank">http://www.u2u.be/software/</a>. Then you can copy the query and use it as a filter. Below is an example with multiple filters allowed of a query you can have.

   <p class="alert alert-success"> If you’re using SharePoint 2013, install the U2U CAML Query Builder for SharePoint 2013 (Standalone Edition).</p>

    <View>
    <Query>
        <Where>
            <And>// have to meet this 2 conditions
                <Eq> //Equal
                    <FieldRef Name='PromotedState' />
                    <Value Type='Number'>2</Value>
                </Eq>
                <Leq> //Lower or Equal
                    <FieldRef Name="Created" />
                    <Value Type="DateTime" Offset="-90"> // -90 days from today
                        <Today />
                    </Value>
                </Leq>
            </And>
        </Where>
        <OrderBy>
            <FieldRef Name='Created' Ascending='False'></FieldRef> // Most Recent to older created element
        </OrderBy>
    </Query>
    <RowLimit>3</RowLimit> // Number of items that you want to show
    </View>