1. Click on **Bindtuning**, and then **Edit Web parts** - the edit panel will appear from the left side of the web part;
2. Mouse hover the tile you want to delete, and click on the 🗑️ (trash) icon;

	![delete_tile.gif](../../images/classic/04.delete_tile.gif)	

3. A message will appear requesting your confirmation. Click **Ok** and the web part will refresh and the tile will be removed.

	![deletetile.png](../../images/classic/20.delete_tile.png)
