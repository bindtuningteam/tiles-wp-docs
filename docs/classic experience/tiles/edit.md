1. Click on **Bindtuning**, and then **Edit Web parts** - the edit panel will appear from the left side of the web part;
2. Mouse hover the tile you want to edit, and click on the ✏️ (pencil) icon;

	![edit_tile.gif](../../images/classic/05.edit_tile.gif)

3. You can check what you can edit in each section on the [Tiles Settings](../../global/tilessettings);
4. Done editing? You can click on the **Preview** button to see how everything looks on the page, click on **Publish**, to save the changes.

	<p class="alert alert-success"><strong>Publish</strong>, <strong>Save as Draft</strong> and <strong>Submit for Review</strong> will only be visible if you previsouly checked the <strong>Enable Publishing Workflow</strong> option while setting the web part properties. You can read more about it <a href="../../workflow" target="_blank">here</a>.</p> 

	![save_buttons.png](../../images/classic/19.save_tile.png)