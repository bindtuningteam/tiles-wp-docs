1. Open the page where you want to add the web part and click on the **Edit** button;
    
    ![edit-page](../images/modern/01.edit.modern.png)
     
2. Click on the **[+]** button to add a new web part or section to your page;


3. On the web part panel, search for **BindTuning** to filter the web parts. Click on the **BindTuning Tiles**;
    
    ![Add section](../images/modern/02.add.section.png)

Now the only thing left to do is to setup the rest of the **[Web Part Properties](./general.md)**.