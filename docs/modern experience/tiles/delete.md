**Option 1**

1. Click on the **Edit** button;

![edit-page](../../images/modern/01.edit.modern.png)

2. Mouse over the tile you intend to delete and click the **Trash** icon to delete the Tile
3. A message will appear requesting your confirmation. Click **Ok** and the web part will refresh and the Tile will be removed.	

![delete tile](../../images/modern/03.delete.tile1.gif)
__________
**Option 2**

1. Click on the **Edit** button;

![edit-page](../../images/modern/01.edit.modern.png)

2. On the web part sidebar and click the **Manage Tiles** icon;

3. The list of Tiles will appear. Click the **Trash** icon to delete the Tile that you want to remove;

4. A message will appear requesting your confirmation. Click **Ok** and the web part will refresh and the Tile will be removed.	

![delete tile](../../images/modern/03.delete.tile2.gif)