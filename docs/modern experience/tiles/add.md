1. Click on the **Edit** button;

	![edit-page](../../images/modern/01.edit.modern.png)

2. Mouse hover the Web Part and click on the **[+]** button to add a new **Tile**;

	![add_tile](../../images/modern/02.add.tile.gif)

3. Fill out the form that pops up. You can check out what you need to do in each setting in the <a href="../../../global/tilessettings" target="_blank">Tiles Settings</a> section;

4. After setting everything up, click on the **Preview** button if you want to see how everything looks on the page, or click on **Save**.

	![19.save_tile](../../images/classic/19.save_tile.png)