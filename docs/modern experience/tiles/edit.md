**Option 1**

1. Click on the **Edit** button;

    ![edit-page](../../images/modern/01.edit.modern.png)

2. Mouse over the tile you intend to edit and click the **pencil**
3. You can check what you can edit in each section on the [Tiles Settings](../../global/tilessettings);	

![edit tile](../../images/modern/04.edit.tile1.gif)
__________
**Option 2**

1. Click on the **Edit** button;

    ![edit-page](../../images/modern/01.edit.modern.png)

2. On the web part sidebar and click the **Manage Tiles** icon;

3. The list of Tiles will appear. Click the **pencil** icon to edit the Tile;

4. You can check what you can edit in each section on the [Tiles Settings](../../global/tilessettings);	

![edit tile](../../images/modern/04.edit.tile2.gif) 