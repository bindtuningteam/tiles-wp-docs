![advanced.PNG](../images/classic/15.weathersettings.png)

<p class="alert alert-warning">The <strong>Weather Underground</strong> API will be shutdown on the 16th of February. We recommend switching to the OpenWeather API instead.</p>
 
____
### Provider

Select the weather provider you want to use

- **OpenWeather** - you can get your key from [here](https://openweathermap.org/appid)  
- **Weather Undeground** (Not Recommended) - you can get your key from [here](https://www.wunderground.com/weather/api/)

____
### API Key

This field is where you should insert the API Key for the weather provider you're using. 
You can aquire a key for each provider by navigating to the links in the section above.

Results from the APIs are cached to prevent the API limits from being reached.
