![Add section](../images/modern/08.advancedoptions.png)

### Title Link 

Note this option is only valid if you have a Title on the Web Part.<br>
On the **Title** of the Web Part, you can add an URL to redirect you to any page. The Web Part will always open the new URL in a new tab of your browser and that option is only visible if you're not in **Edit** mode.

___
### Always Show BT Buttons

When this is **turned on** the icons to manage the content of the Web Part are always visible when the page is in edit mode.<br>
When the feature is **turned off** you need to hover the web part to see the controls.

___
### Title

If you click on the Title of the Web Part on the page, you can change the Title to the Web Part that will be shown. When you insert a text there it'll show on the page the **Title**, otherwise, nothing will be visible in the page.

![02.appearance.text.png](../images/modern/04.appearance.text.png)