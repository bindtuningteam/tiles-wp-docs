![06.options](../images/modern/06.options.png)

On the Web Part Properties panel, you've multiple options which you can edit for diferent configuration of the Web Part.

- [Create New BT Tiles List](./createlist.md)
- [List Settings](./tiles.md)
- [Grid Settings](./grid.md)
- [Weather Settings](./weather.md)
- [Web Part Appearance](./appearance.md)
- [Performance](./performance.md)
- [Advanced Options](./advanced.md)
- [Web Part Messages](./message.md)
 
