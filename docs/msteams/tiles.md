![generaloptions](../images/msteams/01.generaloptions.gif)

### Tiles List

Here you will need to connect your web part with a SharePoint list. Without it, the web part will not function and an notification message will appear. Let's get to it:

1. On **Tiles List**, click **Add List** or **Edit**
2. On the first text box insert one of the two:

	- **The URL of the list you created** 
	- **A URL of a site collection**	 

	<p class="alert alert-warning">Use relative paths for this field. So instead of using an URL like <strong>"https://company.sharepoint.com/sites/Home”</strong>, you should use something like <strong>“/sites/Home/”</strong>.</p>

3. Second box, select the list you have created to be used if not already selected;

4. Choose your **Filtering Options** to filter your list elements. You can choose to have:

    - **No Filters** - You show all the items on the list.
    - **CAML Query** - You can query the list to just show items based on the query built.
    - **Select a View of the list** - You can pick the List View created and just show those elements on the list.


5. When you're done, click on the 💾(save) icon to save.

____
### How to use CAML Query

You can use a free 3rd party software for creating the query, like this one👉<a href="http://www.u2u.be/software/" target="_blank">http://www.u2u.be/software/</a>. Then you can copy the query and use it as a filter. Below is an example with multiple filters allowed of a query you can have.

   <p class="alert alert-success"> If you’re using SharePoint 2013, install the U2U CAML Query Builder for SharePoint 2013 (Standalone Edition).</p>

    <View>
    <Query>
        <Where>
            <And>// have to meet this 2 conditions
                <Eq> //Equal
                    <FieldRef Name='PromotedState' />
                    <Value Type='Number'>2</Value>
                </Eq>
                <Leq> //Lower or Equal
                    <FieldRef Name="Created" />
                    <Value Type="DateTime" Offset="-90"> // -90 days from today
                        <Today />
                    </Value>
                </Leq>
            </And>
        </Where>
        <OrderBy>
            <FieldRef Name='Created' Ascending='False'></FieldRef> // Most Recent to older created element
        </OrderBy>
    </Query>
    <RowLimit>3</RowLimit> // Number of items that you want to show
    </View>

____
### Item Sorting Options

![sorting](../images/modern/12.sorting.png)


<p class="alert alert-info">By default, this option contains the <b>Order By</b> on the column Sequence of the Tiles list, which represent an option to insert a tiles on a page with an certain order. For more information about this option check the <a href="../global/tilessettings">Tiles Settings</a>.</p>

Here you can define what will be the order of your tiles using the internal name of one of your list colunms name. 

You will need to type in the internal name of the list column on the **Order by** text box - we will sort the tiles according to the value of the cell name you enter.

Here is what you need to do: 

1. Access the **Teams Settings**

    ![settings_edit.png](../images/msteams/setting_edit.png)

2. Click to Open the **Tiles List**. 

    ![open_list.png](../images/msteams/open_list.png)

2. On the top menu, click on **List** and then **List Settings** (or **Settings** > **List Settings**);

3. On the **Columns** section, click to open the colum name you want to use;

4. Inside, on the URL look for **"...Field=..."**.  
5. Copy the internal name;

	![webpartproperties_18.png](../images/classic/17.internalcolumnname.png) 
	
6. Now paste the name on the text box;

7. Choose the order, **Ascending** or **Descending**. 

With **Add Order By**, you can add more sorting options. 