![webpartapperence](../images/msteams/07.webpartapperence.png)



###  Rigth to Left

Using this option will change the web part's text orientation to go from right to left. The forms will not be affected.