1. Open the Team on **Teams panel** that you intend to edit an item of the Web Part; 
2. Click on the **Settings** button.

	![settings_delete.png](../../images/msteams/setting_edit.png)

3. Click on the **Manage Tiles** icon;

	![Edit](../../images/classic/04.edit_tile.png)

4. Change the <a href="../../global/tilessettings">Tiles Settings</a> as necessary to re-configure the tiles.
5. Done editing? Click on the **Preview** button if you want to see how everything looks on the page or on **Save Changes**.