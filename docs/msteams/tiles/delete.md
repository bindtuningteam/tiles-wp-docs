1. Open the Team on **Teams panel** that you intend to remove an item of the Web Part;
2. Click on the **Settings** button.

	![settings_delete.png](../../images/msteams/setting_edit.png)

3. Click on the **Manage Tiles** icon;

	![Manage](../../images/msteams/manage_tile.png)

3. The list of Tiles will appear. Click the **Trash** icon to delete the Tile that you want to remove;

	![Delete](../../images/classic/04.delete_tile.png)

4. A message will appear requesting your confirmation. Click **Ok** and the web part will refresh and the Tile will be removed.	

